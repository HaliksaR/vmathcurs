package com.haliksar.vmathapproximation.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import com.haliksar.vmathapproximation.algorithm.Approximation
import com.haliksar.vmathapproximation.R
import com.haliksar.vmathapproximation.classes.Paddings
import com.haliksar.vmathapproximation.classes.Point
import com.haliksar.vmathapproximation.classes.singleton.StArrayPoints
import com.haliksar.vmathapproximation.classes.singleton.StNumDivision

class GraphView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    //Padding's
    private lateinit var paddingData: Paddings
    //Paint
    private lateinit var systemPaint: Paint
    private lateinit var pointPaint: Paint
    private lateinit var lagrangePaint: Paint

    //coordinates
    private var currentPointIndex: Int = 0
    private var xStartSys: Int = 0
    private var xEndSys: Int = 0
    private var yStartSys: Int = 0
    private var yEndSys: Int = 0
    //screen size
    private var mWidth: Int = 0
    private var mHeight: Int = 0

    //steps
    private var divisionStepY: Float = 0f
    private var divisionStepX: Float = 0f

    //data values
    private var POINT_COUNT = StNumDivision.getInstance()!!.division
    private var X_MAX: Int = 5
    private var Y_MAX: Int = 5
    private var stepX: Float = .5f
    private var stepY: Float = .5f
    private var points = arrayOfNulls<Point>(POINT_COUNT)
    private val lagrangePoints = ArrayList<Point>()

    //Assets
    private var touch: Boolean = true

    init {
        initVariables()
    }

    fun initPointsCount(): Boolean {
        X_MAX = StNumDivision.getInstance()!!.division
        Y_MAX = StNumDivision.getInstance()!!.division
        stepX = .5f
        stepY = .5f
        StNumDivision.delInstance()
        return true
    }

    //--------------start----------initVariables----------start--------------
    private fun initVariables() {
        initPaddingData()
        initPaint()
    }

    private fun initPaint() {
        systemPaint = Paint()
        systemPaint.run {
            color = Color.GRAY
            strokeWidth = paddingData.lineSize.toFloat()
            textSize = resources.getDimensionPixelSize(R.dimen.text_size).toFloat()
            textAlign = Paint.Align.CENTER
        }
        pointPaint = Paint()
        pointPaint.run {
            color = ContextCompat.getColor(this@GraphView.context,
                R.color.button_focused
            )
            strokeWidth = resources.getDimensionPixelSize(R.dimen.point_size).toFloat()
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
        }
        lagrangePaint = Paint()
        lagrangePaint.run {
            style = Paint.Style.STROKE
            isAntiAlias = true
            color = Color.CYAN
            strokeWidth = paddingData.lineSize.toFloat()
        }
    }

    private fun initPaddingData() {
        paddingData = Paddings(
            padding = resources.getDimensionPixelSize(R.dimen.axis_padding),
            arrowShift = resources.getDimensionPixelSize(R.dimen.arrow_shift),
            divisionShift = resources.getDimensionPixelSize(R.dimen.division_shift),
            drawingStep = resources.getDimensionPixelSize(R.dimen.drawing_step),
            textShift = resources.getDimensionPixelSize(R.dimen.text_shift),
            textSize = resources.getDimensionPixelSize(R.dimen.text_size),
            lineSize = resources.getDimensionPixelSize(R.dimen.line_size)
        )
    }
    //--------------end----------initVariables----------end--------------

    //--------------start----------onMeasure----------start--------------
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        mWidth = MeasureSpec.getSize(widthMeasureSpec)
        mHeight = MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(mWidth, mHeight)
        xStartSys = paddingData.padding
        xEndSys = mWidth - paddingData.padding
        yStartSys = paddingData.padding
        yEndSys = mHeight - paddingData.padding
        divisionStepY = ((mWidth - paddingData.padding * 2 - paddingData.arrowShift * 2) / Y_MAX).toFloat()
        divisionStepX = ((mWidth - paddingData.padding * 2 - paddingData.arrowShift * 2) / X_MAX).toFloat()
        if (!touch) {
            for (i in points.indices) {
                if (points[i]!!.x == i * stepX) {
                    points[i]!!.x = xStartSys + (divisionStepX * i)
                    points[i]!!.y = (yEndSys - (divisionStepY * points[i]!!.y / stepY))
                    Log.d("mLogs", "points ${points[i]!!.x}, ${points[i]!!.y}")
                }
            }
            touch = true
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }
    //--------------end----------onMeasure----------end--------------

    //--------------start----------onTouchEvent----------start--------------
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                currentPointIndex = getCurrentPointIndex(event.x)
                setCurrentPoint(event.y)
                if (lagrangePoints.isNotEmpty())
                    lagrangeApproximation()
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                setCurrentPoint(event.y)
                if (lagrangePoints.isNotEmpty())
                    lagrangeApproximation()
                invalidate()
            }
        }
        return true
    }

    private fun getCurrentPointIndex(x: Float): Int {
        var rangeStart: Float
        var rangeEnd: Float
        for (i in 0 until POINT_COUNT) {
            if (i == 0) {
                rangeStart = .0f
                rangeEnd = (stepX) * divisionStepX
            } else {
                rangeStart = (i - 0.5f) * divisionStepX
                rangeEnd = (i + 0.5f) * divisionStepX
            }
            if (x >= xStartSys + rangeStart && x < xStartSys + rangeEnd)
                return i
        }
        return -1
    }

    private fun setCurrentPoint(y: Float) {
        if (currentPointIndex != -1 && y > yStartSys && y < yEndSys)
            points[currentPointIndex] =
                Point((divisionStepX * currentPointIndex + xStartSys), y)
    }
    //--------------end----------onTouchEvent----------end--------------

    //--------------start----------onDraw----------start-------------
    override fun onDraw(canvas: Canvas) {
        canvas.drawColor(Color.WHITE)
        drawAxisSystem(canvas)
        if (lagrangePoints.isNotEmpty())
            drawLagrangeGraph(canvas)
        drawPoints(canvas)
    }

    private fun drawAxisSystem(canvas: Canvas) {
        canvas.drawLine( // Y line
            xStartSys.toFloat(),
            yStartSys.toFloat(),
            xStartSys.toFloat(),
            yEndSys.toFloat(),
            systemPaint
        )
        canvas.drawLine( // X line
            xStartSys.toFloat(),
            yEndSys.toFloat(),
            xEndSys.toFloat(),
            yEndSys.toFloat(),
            systemPaint
        )
        canvas.drawLine( // arrow Y
            xStartSys.toFloat(),
            yStartSys.toFloat(),
            (xStartSys + paddingData.arrowShift).toFloat(),
            (yStartSys + paddingData.arrowShift).toFloat(),
            systemPaint
        )
        canvas.drawLine( // arrow Y
            xStartSys.toFloat(),
            yStartSys.toFloat(),
            (xStartSys - paddingData.arrowShift).toFloat(),
            (yStartSys + paddingData.arrowShift).toFloat(),
            systemPaint
        )
        canvas.drawLine( // arrow x
            xEndSys.toFloat(),
            yEndSys.toFloat(),
            (xEndSys - paddingData.arrowShift).toFloat(),
            (yEndSys - paddingData.arrowShift).toFloat(),
            systemPaint
        )
        canvas.drawLine( // arrow x
            xEndSys.toFloat(),
            yEndSys.toFloat(),
            (xEndSys - paddingData.arrowShift).toFloat(),
            (yEndSys + paddingData.arrowShift).toFloat(),
            systemPaint
        )
        var startX: Int
        var startY: Int
        var stopX: Int
        var stopY: Int
        for (i in 0..((mWidth - paddingData.padding * 2) / divisionStepX).toInt()) { // draw text X
            startX = (xStartSys + divisionStepX * i).toInt()
            startY = yEndSys - paddingData.divisionShift
            stopX = (xStartSys + divisionStepX * i).toInt()
            stopY = yEndSys + paddingData.divisionShift
            canvas.drawLine(
                startX.toFloat(),
                startY.toFloat(),
                stopX.toFloat(),
                stopY.toFloat(),
                systemPaint
            )
            canvas.drawText(
                String.format("%.2f", i * stepX),
                startX.toFloat(),
                (stopY + paddingData.textShift + paddingData.textSize / 2).toFloat(),
                systemPaint
            )
        }
        val skip: Int = if (Y_MAX.div(1 / stepY).toInt() < 10) 1
        else Y_MAX.div(1 / stepY).toInt()
        for (i in 0..((mHeight - paddingData.padding * 2) / divisionStepY).toInt() step skip) { // draw text Y
            startX = xStartSys - paddingData.divisionShift
            startY = (yEndSys - divisionStepY * i).toInt()
            stopX = xStartSys + paddingData.divisionShift
            stopY = (yEndSys - divisionStepY * i).toInt()

            canvas.drawLine(
                startX.toFloat(),
                startY.toFloat(),
                stopX.toFloat(),
                stopY.toFloat(),
                systemPaint
            )
            if (i != 0) canvas.drawText(
                String.format("%.2f", i * stepY),
                (startX - paddingData.textShift).toFloat(),
                (stopY + paddingData.textSize / 2).toFloat(),
                systemPaint
            )
        }
    }

    private fun drawPoints(canvas: Canvas) {
        for (point in points) {
            if (point != null) {
                if (point.x != Float.NaN) {
                    canvas.drawPoint(
                        point.x,
                        point.y,
                        pointPaint
                    )
                    canvas.drawText(
                        String.format("x = %.2f", (point.x / divisionStepX).toInt() * stepX),
                        point.x + paddingData.textShift,
                        point.y - paddingData.textShift,
                        systemPaint
                    )
                    canvas.drawText(
                        String.format("y = %.2f", (yEndSys - point.y) / divisionStepY * stepY),
                        point.x + paddingData.textShift,
                        point.y - paddingData.textShift - paddingData.textSize,
                        systemPaint
                    )
                }
            }
        }
    }

    private fun drawLagrangeGraph(canvas: Canvas) {
        for (i in 1 until lagrangePoints.size) {
            canvas.drawLine(
                lagrangePoints[i - 1].x,
                lagrangePoints[i - 1].y,
                lagrangePoints[i].x,
                lagrangePoints[i].y,
                lagrangePaint
            )
        }
    }
    //--------------end----------onDraw----------end--------------

    //--------------start----------lagrangeApproximation----------start-------------
    fun lagrangeApproximation() {
        lagrangePoints.clear()
        val relativeCoordinates = getRelativeCoordinates(points)
        var x = xStartSys.toFloat()
        var y: Float
        do {
            y = Approximation.lagrangePolynomial(getRelativeX(x), relativeCoordinates)
            lagrangePoints.add(Point(x, getAbsoluteY(y)))
            x += paddingData.drawingStep.toFloat()
        } while (x < xStartSys + divisionStepX * Y_MAX)
    }

    private fun getRelativeCoordinates(points: Array<Point?>): Array<Point?> {
        val relativePoints = ArrayList<Point>()
        var x: Float
        var y: Float
        for (i in points.indices) {
            if (points[i]!!.x != Float.NaN || points[i]!!.y != Float.NaN) {
                x = (points[i]!!.x - xStartSys) / divisionStepX
                y = (yEndSys - points[i]!!.y) / divisionStepY
                relativePoints.add(Point(x, y))
            }
        }
        val returnArray = arrayOfNulls<Point>(relativePoints.size)
        for (i in returnArray.indices)
            returnArray[i] = relativePoints[i]
        return returnArray
    }

    private fun getRelativeX(x: Float): Float = (x - xStartSys) / divisionStepX
    private fun getAbsoluteY(y: Float): Float = yEndSys - y * divisionStepY
    //--------------end----------lagrangeApproximation----------end--------------

    //--------------start----------Assets----------start-------------
    fun clear() {
        lagrangePoints.clear()
        points = arrayOfNulls(POINT_COUNT)
        invalidate()
    }

    fun checkPointCount(): Boolean {
        for (point in points)
            if (point == null) return false
        return true
    }

    fun checkInstance(): Boolean {
        touch = false
        invalidate()
        val st = StArrayPoints.getInstance() ?: return false
        stepX = st.step
        if (st.array == null)
            return false
        X_MAX = st.array!!.size
        for (i in st.array!!.indices) {
            if (X_MAX < st.array!![i].x)
                X_MAX = st.array!![i].x.toInt() + 1
            if (Y_MAX < st.array!![i].y)
                Y_MAX = st.array!![i].y.toInt() + 1
            if (stepY > st.array!![i].y.rem(st.array!![i].y.toInt()) && st.array!![i].y.rem(st.array!![i].y.toInt()) != 0f)
                stepY = st.array!![i].y.rem(st.array!![i].y.toInt())
        }
        Y_MAX = if (Y_MAX.rem(stepY) == .0f) (Y_MAX / stepY).toInt()
        else (Y_MAX / stepY).toInt() + 1
        points = Array(size = POINT_COUNT, init = {
            Point(Float.NaN, Float.NaN)
        })
        var k = 0
        for (i in st.array!!.indices) {
            if (st.array!![i].x == i * stepX) {
                points[k]!!.x = st.array!![i].x
                points[k]!!.y = st.array!![i].y
                k++
            }
        }
        return true
    }
    //--------------end----------Assets----------end--------------
}
