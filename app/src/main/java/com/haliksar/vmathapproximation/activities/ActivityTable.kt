package com.haliksar.vmathapproximation.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.haliksar.vmathapproximation.classes.Point
import com.haliksar.vmathapproximation.R
import com.haliksar.vmathapproximation.classes.singleton.StNumDivision

class ActivityTable : AppCompatActivity() {

    @BindView(R.id.editTextY)
    lateinit var editTextY: EditText

    @BindView(R.id.editTextX)
    lateinit var stepX: EditText

    private lateinit var points: ArrayList<Point>
    private var step: Float? = null

    @OnClick(R.id.ButtonStart)
    fun onClickButton() {
        if (checkData()) {
            StNumDivision.delInstance()
            StNumDivision.getInstance(points.size)
            startActivity(
                Intent(this, ActivityGraph::class.java)
                    .putParcelableArrayListExtra("points", points)
                    .putExtra("step", step)
            )
            finish()
        }
    }

    private fun checkData(): Boolean {
        if (editTextY.text.isNotEmpty() && stepX.text.isNotEmpty()) {
            val listsY = editTextY.text.toString().split(",")
            if (listsY.size > 2) {
                points = ArrayList(listsY.size)
                step = stepX.text.toString().toFloat()
                for (i in listsY.indices)
                    points.add(
                        Point(
                            x = i.toFloat() * step!!,
                            y = listsY[i].toFloat()
                        )
                    )
                return true
            } else Toast.makeText(this, "Мало точек!", Toast.LENGTH_LONG).show()
        } else Toast.makeText(this, "Пустое поле!", Toast.LENGTH_LONG).show()
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_table)
        ButterKnife.bind(this)
    }
}
