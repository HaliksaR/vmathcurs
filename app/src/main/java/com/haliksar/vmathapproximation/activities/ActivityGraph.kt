package com.haliksar.vmathapproximation.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.haliksar.vmathapproximation.*
import com.haliksar.vmathapproximation.classes.Point
import com.haliksar.vmathapproximation.classes.singleton.StArrayPoints
import com.haliksar.vmathapproximation.classes.singleton.StNumDivision
import com.haliksar.vmathapproximation.views.GraphView

class ActivityGraph : AppCompatActivity() {

    @BindView(R.id.graphView)
    lateinit var graphView: GraphView

    @OnClick(R.id.btnLagrangePolynomial)
    fun onClickLagrangePolynomial() {
        if (graphView.checkPointCount()) {
            graphView.lagrangeApproximation()
            graphView.invalidate()
            Log.d("mLogs", "invalidate()")
        } else
            Toast.makeText(this, resources.getString(R.string.define), Toast.LENGTH_SHORT).show()
    }

    @OnClick(R.id.btnBack)
    fun onClickBack() {
        StArrayPoints.delInstance()
        graphView.clear()
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_graph)
        ButterKnife.bind(this)
        val points = intent.getParcelableArrayListExtra<Point>("points")
        val step = intent.getFloatExtra("step", -1f)
        if (points != null) {
            val array = Array(size = points.size, init = {
                Point(0f, 0f)
            })
            for (i in points.indices)
                array[i] = points[i]
            StArrayPoints.getInstance(array, step)
            if (!graphView.checkInstance())
                Toast.makeText(this, resources.getString(R.string.define), Toast.LENGTH_SHORT).show()
        } else {
            if (StNumDivision.getInstance() != null) {
                if (!graphView.initPointsCount()) {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }
        }
    }

    override fun onBackPressed() {
        graphView.clear()
        StNumDivision.delInstance()
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        graphView.clear()
        StNumDivision.delInstance()
        finish()
    }
}
