package com.haliksar.vmathapproximation.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.haliksar.vmathapproximation.R
import com.haliksar.vmathapproximation.classes.singleton.StNumDivision

class MainActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {

    private lateinit var textView: TextView
    private lateinit var seekBar: SeekBar

    @OnClick(R.id.ButtonGraph)
    fun onClickButtonGraph() {
        if (textView.text.toString() != getString(R.string.numDivision)) {
            StNumDivision.getInstance(textView.text.toString().toInt())
            startActivity(Intent(this, ActivityGraph::class.java))
        }
    }

    @OnClick(R.id.ButtonSetPoints)
    fun onClickButtonSetPoints() {
        startActivity(Intent(this, ActivityTable::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        textView = findViewById(R.id.textView2)
        seekBar = findViewById(R.id.seekBar)
        seekBar.setOnSeekBarChangeListener(this)
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        textView.text = progress.toString()
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {}
    override fun onStopTrackingTouch(seekBar: SeekBar) {}
    override fun onResume() {
        super.onResume()
        seekBar.progress = 3
        textView.text = getString(R.string.numDivision)
    }
}
