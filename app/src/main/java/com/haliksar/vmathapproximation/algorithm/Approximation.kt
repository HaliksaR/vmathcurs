package com.haliksar.vmathapproximation.algorithm

import com.haliksar.vmathapproximation.classes.Point

object Approximation {
    fun lagrangePolynomial(a: Float, points: Array<Point?>): Float {
        var result = 0f
        var product: Float
        for (i in points.indices) {
            product = 1f
            for (j in points.indices)
                if (j != i)
                    product *= (a - points[j]!!.x) / (points[i]!!.x - points[j]!!.x)
            result += points[i]!!.y * product
        }
        return result
    }
}
