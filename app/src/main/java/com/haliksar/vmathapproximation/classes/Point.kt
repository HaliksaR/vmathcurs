package com.haliksar.vmathapproximation.classes

import android.os.Parcel
import android.os.Parcelable

class Point(x: Float, y: Float) : Parcelable {
    var x: Float = 0f
    var y: Float = 0f

    constructor(parcel: Parcel) : this(
        parcel.readFloat(),
        parcel.readFloat()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(x)
        parcel.writeFloat(y)
    }

    init {
        this.x = x
        this.y = y
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Point> {
        override fun createFromParcel(parcel: Parcel): Point =
            Point(parcel)
        override fun newArray(size: Int): Array<Point?> = arrayOfNulls(size)
    }
}