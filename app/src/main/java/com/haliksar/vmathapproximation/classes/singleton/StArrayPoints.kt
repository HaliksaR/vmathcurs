package com.haliksar.vmathapproximation.classes.singleton

import com.haliksar.vmathapproximation.classes.Point

class StArrayPoints private constructor(var array: Array<Point>?, var step: Float) {
    companion object {
        private var instance: StArrayPoints? = null
        fun getInstance(array: Array<Point>?, step: Float): StArrayPoints? {
            if (instance == null)
                instance =
                    StArrayPoints(array, step)
            return instance
        }
        fun getInstance(): StArrayPoints? {
            return instance
        }
        fun delInstance() {
            instance = null
        }
    }
}