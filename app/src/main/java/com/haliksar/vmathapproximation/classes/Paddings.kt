package com.haliksar.vmathapproximation.classes

data class Paddings(
    val padding: Int,
    val arrowShift: Int,
    val divisionShift: Int,
    val textShift: Int,
    val textSize: Int,
    val drawingStep: Int,
    val lineSize: Int
)