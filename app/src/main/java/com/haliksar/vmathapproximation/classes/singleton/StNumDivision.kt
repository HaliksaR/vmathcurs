package com.haliksar.vmathapproximation.classes.singleton

class StNumDivision private constructor(var division: Int) {
    companion object {
        private var instance: StNumDivision? = null
        fun getInstance(division: Int): StNumDivision? {
            if (instance == null)
                instance =
                    StNumDivision(division)
            return instance
        }
        fun getInstance(): StNumDivision? {
            return instance
        }
        fun delInstance() {
            instance = null
        }
    }
}